var gulp = require('gulp');
// var concat = require('gulp-concat');
var sass = require('gulp-sass');
var rename = require('gulp-rename');



var paths = {
    sass: ['../app/profile/tabs/user/*.scss'],   
};


gulp.task('build', ['sass']);

gulp.task('sass', function(done) {
    gulp.src('../app/profile/tabs/user/user.scss')
        .pipe(sass({
            errLogToConsole: true
        }))
        .pipe(rename({ extname: '.css' }))
        .pipe(gulp.dest('../lib/css/'))       
        .on('end', done);
});

gulp.task('sass', function () {
    return gulp.src('all.scss') // Gets all files ending with .scss in app/scss and children dirs
      .pipe(sass())
      .pipe(gulp.dest('./lib/css'))// desttination path of save file as all.css 
});


var browserSync = require('browser-sync').create();

gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: ''
    },
  })
})