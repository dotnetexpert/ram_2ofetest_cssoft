﻿app.factory('zmProfileUserRestService', ['$http', '$q', function ($http, $q) {
    var zmProfileUserRestService = {};
    var serviceBase = window.location.protocol + '//' + window.location.host + '/';    
    var _readListOfUser = function () {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: serviceBase + "contacts.json",
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    var _readUser = function (userId) {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: serviceBase + "contacts.json",
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    var _deleteUser = function (userId) {
        var deferred = $q.defer();
        $http({
            method: 'DELETE',
            url: serviceBase + "contacts.json",
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    var _addUser = function (user) {
        var deferred = $q.defer();
        $http({
            method: 'PUT',
            url: serviceBase + "contacts.json",
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    var _readPersonalDetails = function (userId) {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: serviceBase + "contacts.json",
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    var _updatePersonalDetails = function (user,userId) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: serviceBase + "contacts.json",
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    var _readCompanyDetails = function (userId) {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: serviceBase + "contacts.json",
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    var _updateCompanyDetails = function (user,userId) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: serviceBase + "contacts.json",
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    zmProfileUserRestService.readListOfUser = _readListOfUser
    zmProfileUserRestService.readUser = _readUser
    zmProfileUserRestService.deleteUser = _deleteUser
    zmProfileUserRestService.addUser = _addUser
    zmProfileUserRestService.readPersonalDetails = _readPersonalDetails
    zmProfileUserRestService.updatePersonalDetails = _updatePersonalDetails
    zmProfileUserRestService.readCompanyDetails = _readCompanyDetails
    zmProfileUserRestService.updateCompanyDetails = _updateCompanyDetails

    return zmProfileUserRestService;

}]);