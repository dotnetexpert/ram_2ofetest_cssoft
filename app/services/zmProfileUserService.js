﻿(function () {
    'use strict';
    var app = angular.module('app.profile')
        //app.service('zmProfileUserService', function ($http, $q) {

        //    var seletedUser = [];
        //    var getListOfUser = function () {
        //        var deferred = $q.defer();
        //        //http.get('../contacts.json');
        //        $http.get('../contacts.json'
        //        ).success(function (data) {
        //            deferred.resolve(data);
        //        }).error(function (error) {
        //            deferred.reject(error);
        //            console.log("error");
        //        });
        //        return deferred.promise;
        //    }

        //    var setSelectedUserddata = function (data) {
        //        seletedUser = [];
        //        seletedUser.push(data);
        //    }

        //    var getSelectedUserddata = function myfunction() {
        //        return seletedUser;
        //    }
        //    return {
        //        getListOfUser: getListOfUser,
        //        setSelectedUserddata: setSelectedUserddata,
        //        getSelectedUserddata: getSelectedUserddata
        //    };
        //});



    app.factory('zmProfileUserService', ["zmProfileUserRestService", '$q', 'localStorageService', function (zmProfileUserRestService, $q, localStorageService) {
            var zmProfileUserService = {};

            var _readListOfUser = function () {
                var deferred = $q.defer();                
                zmProfileUserRestService.readListOfUser().then(function (response) {                    
                    var userData = localStorageService.get('userList');
                    deferred.resolve(userData);
                },
                function (error) {
                    deferred.reject(error);
                })
                return deferred.promise;
            }
            var _readUser = function (userId) {
                var deferred = $q.defer();

                zmProfileUserRestService.readUser().then(function (response) {
                    deferred.resolve(response);
                },
                function (error) {
                    deferred.reject(error);
                })
                return deferred.promise;
            }
            var _deleteUser = function (userId) {
                var deferred = $q.defer();               

                var userData = localStorageService.get('userList');                
                userData.data.splice(userId, 1);
                localStorageService.set('userList', userData)

                deferred.resolve(userData);
                
                return deferred.promise;
            }
            var _addUser = function (user) {
                debugger;
                var deferred = $q.defer();

                var userData = localStorageService.get('userList');
                userData.data.unshift(user);
                localStorageService.set('userList', userData)
                deferred.resolve(userData);
                return deferred.promise;
            }
            var _readPersonalDetails = function (userId) {
                var deferred = $q.defer();

                zmProfileUserRestService.readPersonalDetails().then(function (response) {
                    deferred.resolve(response);
                },
                function (error) {
                    deferred.reject(error);
                })
                return deferred.promise;
            }
            var _updatePersonalDetails = function (user) {
                var deferred = $q.defer();
                debugger;
                var userData = localStorageService.get('userList');
                userData.data[user.userDetail.index].username = user.userDetail.username;
                userData.data[user.userDetail.index].company = user.userDetail.company;
                userData.data[user.userDetail.index].job = user.userDetail.job;
                userData.data[user.userDetail.index].location = user.userDetail.location;
                userData.data[user.userDetail.index].province = user.userDetail.province;
                localStorageService.set('userList', userData)
                deferred.resolve(userData);
                return deferred.promise;
            }
            var _readCompanyDetails = function (userId) {
                var deferred = $q.defer();

                zmProfileUserRestService.readCompanyDetails().then(function (response) {
                    deferred.resolve(response);
                },
                function (error) {
                    deferred.reject(error);
                })
                return deferred.promise;
            }
            var _updateCompanyDetails = function (user) {
                var deferred = $q.defer();                
                var userData = localStorageService.get('userList');
                userData.data[user.companyDetail.index].username = user.companyDetail.username;
                userData.data[user.companyDetail.index].company = user.companyDetail.company;
                userData.data[user.companyDetail.index].job = user.companyDetail.job;
                userData.data[user.companyDetail.index].location = user.companyDetail.location;
                userData.data[user.companyDetail.index].province = user.companyDetail.province;
                localStorageService.set('userList', userData)
                deferred.resolve(userData);
                return deferred.promise;
            }

            zmProfileUserService.readListOfUser = _readListOfUser
            zmProfileUserService.readUser = _readUser
            zmProfileUserService.deleteUser = _deleteUser
            zmProfileUserService.addUser = _addUser
            zmProfileUserService.readPersonalDetails = _readPersonalDetails
            zmProfileUserService.updatePersonalDetails = _updatePersonalDetails
            zmProfileUserService.readCompanyDetails = _readCompanyDetails
            zmProfileUserService.updateCompanyDetails = _updateCompanyDetails

            return zmProfileUserService;
        }]);
})();


