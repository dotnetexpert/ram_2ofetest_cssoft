﻿function ZmProfileUserDetailController($scope, $rootScope, $mdDialog) {
    var vm = this;
    vm.selected = false;
    vm.editContact = $rootScope.editContact;
    vm.DeleteUserDialog = DeleteUserDialog;
    vm.SavePersonnelDetail = SavePersonnelDetail;
    vm.SaveCompanyDetail = SaveCompanyDetail;

    $scope.$on('selectedUserDetail', function (event, args) {                    
        vm.selected = args;
        vm.editContact = false;
        console.log(args);
    });
    $scope.$on('editSelectedUserDetail', function (event, args) {
        vm.editContact = true;
        vm.selected = args;
        console.log(args);
        
    });

    // function to delete user
    function DeleteUserDialog(ev, id) {
        $mdDialog.show({
            controller: 'DeleteUserDialogController',
            controllerAs: 'vm',
            templateUrl: 'app/profile/dialogs/delete-user/delete-user.html',
            targetEvent: ev,
            clickOutsideToClose: true
        }).then(function (val) {
            if (val == 1) {
                $mdDialog.hide();
                $scope.$emit('selectedUserToDelete', { userid: id, val: val });
            }
        }, function () {
        });
    }
    function SavePersonnelDetail(userDetail)
    {
        $scope.$emit('updateUserDetail', { userDetail: userDetail });
        vm.editContact = false;
    }
    function SaveCompanyDetail(companyDetail)
    {
        $scope.$emit('updateCompanyDetail', { companyDetail: companyDetail });
        vm.editContact = false;               
    }
}
angular.module('app.profile').controller('ZmProfileUserDetailController', ['$scope', '$rootScope', '$mdDialog', ZmProfileUserDetailController]);

angular.module('app.profile').component('zmProfileUserDetail', {
    templateUrl: 'app/profile/tabs/user/component/userdetail.component/zmprofileuserdetail.html',
    controller: 'ZmProfileUserDetailController'
});