﻿function ZmProfileUserController($scope, $mdToast, $rootScope, $mdDialog, $document, zmProfileUserService) {
    var vm = this;
    vm.AddUserDialog = AddUserDialog;
    vm.DeleteUserDialog = DeleteUserDialog;
    vm.getSelectedUser = getSelectedUser;
    vm.editSelectedUser = editSelectedUser;

    $rootScope.selected = "";
    $rootScope.editContact = false;

    // service to get list of users.
    zmProfileUserService.readListOfUser().then(function (response) {
        vm.List = response.data;
    });

    // function to get selected value of user.
    function getSelectedUser(entry, index) {
        vm.selected = entry;
        entry.index = index;
        $scope.$emit('selectedUserData', entry);
    }

    // function to edit selected user.
    function editSelectedUser(e, entry, index) {
        vm.selected = entry;
        entry.index = index;
        $scope.$emit('editSelectedUserData', entry);
        e.stopPropagation();
    }

    // function to add user values.
    function AddUserDialog(ev) {
        var parentEl = angular.element(document.body);
        $mdDialog.show({
            parent: parentEl,
            scope: $scope,
            preserveScope: true,
            controller: AddUserDialogController,
            templateUrl: 'app/profile/dialogs/add-user/add-user.html',
            targetEvent: ev,
            clickOutsideToClose: false,
            locals: { employee: 0 }
        });
        function AddUserDialogController($scope, $mdDialog, employee) {
            $scope.employee = employee;
            $scope.closeDialog = function () {
                $mdDialog.hide();
            };
            $scope.addUser = function (name) {



                var user = {
                    "username": name,
                    "userid": vm.List.length + 1,
                    "company": "Zippel Media",
                    "job": "Developer",
                    "location": "Elsdorf", "province": "North-Rhine-Westfalia",
                    "avatar": "./images/no-user-avtar.png",
                    "type": "user"
                }
                zmProfileUserService.addUser(user).then(function (response) {
                    vm.List.unshift(user);
                });


                $mdDialog.hide();
                $mdToast.show(
                    $mdToast.simple()
                    .content("User added successfully")
                    .action('Ok')
                    .highlightAction(false)
                    .position('center')
                    );
            };
        }
    }
    // function to delete user.
    function DeleteUserDialog(ev, id) {
        $mdDialog.show({
            controller: 'DeleteUserDialogController',
            controllerAs: 'vm',
            templateUrl: 'app/profile/dialogs/delete-user/delete-user.html',
            targetEvent: ev,
            clickOutsideToClose: true
        }).then(function (val) {
            if (val == 1) {
                vm.List.splice(id, 1);
                $mdToast.show(
                       $mdToast.simple()
                       .content("User deleted successfully")
                       .action('Ok')
                       .highlightAction(false)
                       .position('center')
                    );
            }
        }, function () {
        });
    }

    // Catch deleteSelectedUser event. 
    $scope.$on('deleteSelectedUser', function (event, args) {
        if (args.val == 1) {
            zmProfileUserService.deleteUser(args.userid).then(function (response) {
                // vm.List.unshift(user);
            });
            vm.List.splice(args.userid, 1);
            $mdToast.show(
                   $mdToast.simple()
                   .content("User deleted successfully")
                   .action('Ok')
                   .highlightAction(false)
                   .position('center')
                );
        }
    });
    $scope.$on('updateUserCompanyDetail', function (event, args) {
        alert(JSON.stringify(args) + 'sdfsfsdf');
        zmProfileUserService.updateCompanyDetails(args).then(function (response) {
            vm.List = response.data;
        });               
    });
    $scope.$on('updateUserProfileDetail', function (event, args) {
        alert(JSON.stringify(args) + 'sdfsfsdf');
        zmProfileUserService.updatePersonalDetails(args).then(function (response) {
            vm.List = response.data;
        });
    });

}
angular.module('app.profile').controller('ZmProfileUserController', ['$scope', '$mdToast', '$rootScope', '$mdDialog', '$document', 'zmProfileUserService', ZmProfileUserController]);

angular.module('app.profile').component('zmProfileUser', {
    templateUrl: 'app/profile/tabs/user/component/user.component/zmprofileuser.html',
    controller: 'ZmProfileUserController'
});

