(function () {
    'use strict';
    app.controller('AddUserDialogController', AddUserDialogController);

    function AddUserDialogController($mdDialog, $scope) {

        var vm = this;
        vm.closeDialog = closeDialog;
        vm.addUser = addUser;
        function closeDialog() {
            $mdDialog.hide();
        };
        function addUser() {            
            $mdDialog.hide();
        };
    }
    
})();
