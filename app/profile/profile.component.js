(function () {
    'use strict';
    angular.module('app.profile')
        .controller('ProfileController', ProfileController);

    function ProfileController($scope, $rootScope, $mdDialog, $document, localStorageService) {
        var vm = this;
        // function to show selected user.
        $scope.$on('selectedUserData', function (event, args) {
            $scope.$broadcast('selectedUserDetail', args);
        });
        // function to edit selected user.
        $scope.$on('editSelectedUserData', function (event, args) {
            $scope.$broadcast('editSelectedUserDetail', args);
        });
        // function to delete selected user.
        $scope.$on('selectedUserToDelete', function (event, args) {
            $scope.$broadcast('deleteSelectedUser', args);
        });
        $scope.$on('updateCompanyDetail', function (event, args) {
            $scope.$broadcast('updateUserCompanyDetail', args);
        });
        $scope.$on('updateUserDetail', function (event, args) {
            $scope.$broadcast('updateUserProfileDetail', args);
        });
        vm.selected = false;
        vm.editContact = false
        var init = function () {           
            var data = {
                "data": [
{
    "username": "Garry Muster",
    "userid": 1,
    "company": "Zippel Media",
    "job": "Developer",
    "location": "Elsdorf",
    "province": "North-Rhine-Westfalia",
    "avatar": "./images/no-user-avtar.png",
    "type": "user"
},
{
    "username": "Alice Muster",
    "userid": 2,
    "company": "Zippel Media",
    "job": "Developer",
    "location": "Elsdorf",
    "province": "North-Rhine-Westfalia",
    "avatar": "./images/no-user-avtar.png",
    "type": "user"
},
{
    "username": "Carl Muster",
    "userid": 3,
    "company": "Zippel Media",
    "job": "Developer",
    "location": "Elsdorf",
    "province": "North-Rhine-Westfalia",
    "avatar": "./images/no-user-avtar.png",
    "type": "user"
},
{
    "username": "Danielle Muster",
    "userid": 4,
    "company": "Zippel Media",
    "job": "Developer",
    "location": "Elsdorf",
    "province": "North-Rhine-Westfalia",
    "avatar": "./images/no-user-avtar.png",
    "type": "user"
},
{
    "username": "Andrew Muster",
    "userid": 5,
    "company": "Zippel Media",
    "job": "Developer",
    "location": "Elsdorf",
    "province": "North-Rhine-Westfalia",
    "avatar": "./images/no-user-avtar.png",
    "type": "user"
},
{
    "username": "James Muster",
    "userid": 6,
    "company": "Zippel Media",
    "job": "Developer",
    "location": "Elsdorf",
    "province": "North-Rhine-Westfalia",
    "avatar": "./images/no-user-avtar.png",
    "type": "user"
},
{
    "username": "Jane Muster",
    "userid": 7,
    "company": "Zippel Media",
    "job": "Developer",
    "location": "Elsdorf",
    "province": "North-Rhine-Westfalia",
    "avatar": "./images/no-user-avtar.png",
    "type": "user"
},
{
    "username": "Joyce Muster",
    "userid": 8,
    "company": "Zippel Media",
    "job": "Developer",
    "location": "Elsdorf",
    "province": "North-Rhine-Westfalia",
    "avatar": "./images/no-user-avtar.png",
    "type": "user"
},
{
    "username": "Katherine Muster",
    "userid": 9,
    "company": "Zippel Media",
    "job": "Developer",
    "location": "Elsdorf",
    "province": "North-Rhine-Westfalia",
    "avatar": "./images/no-user-avtar.png",
    "type": "user"
},
{
    "username": "Vincent Muster",
    "userid": 10,
    "company": "Zippel Media",
    "job": "Developer",
    "location": "Elsdorf",
    "province": "North-Rhine-Westfalia",
    "avatar": "./images/no-user-avtar.png",
    "type": "user"
}
                ]
            };
            if(localStorageService.get("userList") == null)
            {                
                localStorageService.set("userList", JSON.stringify(data));
            }
            
        }

        init();
    }
    angular.module('app.profile')
     .controller('ProfileController', ['$scope', '$rootScope', '$mdDialog', '$document', 'localStorageService', ProfileController]);
})();
