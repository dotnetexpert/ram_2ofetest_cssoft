var app = angular.module('app.profile', ['ngMaterial', 'ui.router','LocalStorageModule'])
.config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {

 $stateProvider
  .state('/', {
   url: '/',
            templateUrl: '../app/profile/profile.html',
            controller : 'ProfileController as vm'
  });

 $urlRouterProvider.otherwise('/');
}]);
